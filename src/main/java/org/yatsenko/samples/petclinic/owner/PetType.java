package org.yatsenko.samples.petclinic.owner;

import org.yatsenko.samples.petclinic.model.NamedEntity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "types")
public class PetType extends NamedEntity {

}
