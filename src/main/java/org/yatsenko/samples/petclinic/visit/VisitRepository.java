package org.yatsenko.samples.petclinic.visit;

import org.springframework.dao.DataAccessException;
import org.springframework.data.repository.Repository;
import org.yatsenko.samples.petclinic.model.BaseEntity;

import java.util.List;


public interface VisitRepository extends Repository<Visit, Integer> {

	/**
	 * Save a <code>Visit</code> to the data store, either inserting or updating it.
	 *
	 * @param visit the <code>Visit</code> to save
	 * @see BaseEntity#isNew
	 */
	void save(Visit visit) throws DataAccessException;

	List<Visit> findByPetId(Integer petId);

}
