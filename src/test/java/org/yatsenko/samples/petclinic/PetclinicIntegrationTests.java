

package org.yatsenko.samples.petclinic;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.yatsenko.samples.petclinic.vet.VetRepository;

@SpringBootTest
@Disabled
class PetclinicIntegrationTests {

	@Autowired
	private VetRepository vets;

	@Test
	void testFindAll() throws Exception {
		vets.findAll();
		vets.findAll(); // served from cache
	}

}
