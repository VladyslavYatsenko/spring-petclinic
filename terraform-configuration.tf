#Building Server
provider "aws" {
  access_key = "AKIA4ESNZVXFMZQLA2U3"
  secret_key = "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"
  region = "us-east-2"
}

resource "aws_instance" "Terraform_CoreOS_Server" {
  ami = "ami-0d1579b60bb706fb7"
  #CoreOS Server AMI
  instance_type = "t2.micro"
  key_name = "aws-key-petclinic"
  tags = {
    Name = "TERRAFORM_CORE_OS_SERVER"
  }
  vpc_security_group_ids = [
    aws_security_group.Terraform_CoreOS_Server.id]
  user_data = file("app-cloud-configuration.yaml")
}

resource "aws_security_group" "Terraform_CoreOS_Server" {
  name = "CoreOs Server Terraform Security Group"
  ingress {
    #incoming
    from_port = 80
    protocol = "tcp"
    to_port = 80
    cidr_blocks = [
      "0.0.0.0/0"]
  }
  ingress {
    from_port = -1
    protocol = "icmp"
    to_port = -1
  }
  ingress {
    #incoming
    from_port = 8060
    protocol = "tcp"
    to_port = 8060
    cidr_blocks = [
      "0.0.0.0/0"]
  }
  ingress {
    from_port = 19531
    protocol = "tcp"
    to_port = 19531
    cidr_blocks = [
      "0.0.0.0/0"]
  }
  ingress {
    from_port = 9001
    protocol = "tcp"
    to_port = 9001
    cidr_blocks = [
      "0.0.0.0/0"]
  }
  ingress {
    from_port = 9000
    protocol = "tcp"
    to_port = 9000
    cidr_blocks = [
      "0.0.0.0/0"]
  }
  ingress {
    #incoming
    from_port = 22
    protocol = "tcp"
    to_port = 22
    cidr_blocks = [
      "0.0.0.0/0"]
  }
  egress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = [
      "0.0.0.0/0"]
  }
}
